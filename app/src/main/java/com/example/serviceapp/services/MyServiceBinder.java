package com.example.serviceapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.util.Log;

import java.lang.ref.PhantomReference;

public class MyServiceBinder extends Service {
    private DownLoadBinder binder = new DownLoadBinder();
    public MyServiceBinder() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MyServiceBinder","onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MyServiceBinder","onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MyServiceBinder","onDestroy");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
        return binder;
    }

    public class DownLoadBinder extends Binder {
        public void startDownLoad(){
            Log.d("MyServiceBinder","startDownLoad executed");
        }

        public int getProgress(){
            Log.d("MyServiceBinder","getProgress executed");
            return 0;
        }


    }
}