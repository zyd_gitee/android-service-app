package com.example.serviceapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

/**
 * 在服务里用线程
 *
 */
public class MyIntentService extends IntentService {
    public MyIntentService() {
        super("MyIntentService"); // 调用父类的有参构造函数
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MyIntentService","onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MyIntentService","onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MyIntentService","onDestroy");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        // 打印当前线程的id
        Log.d("MyIntentService", "onHandleIntent Thread id is " + Thread.currentThread(). getId());
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

//    public class MyService extends Service {
//            ...
//    这种服务一旦启动之后，就会一直处于运行状态，必须调用stopService()或者stopSelf()方法才能让服务停止下来。
//    虽说这种写法并不复杂，但是总会有一些程序员忘记开启线程，或者忘记调用stopSelf()方法。
//    为了可以简单地创建一个异步的、会自动停止的服务，Android专门提供了一个IntentService类，
//    这个类就很好地解决了前面所提到的两种尴尬。
//        @Override
//        public int onStartCommand(Intent intent, int flags, int startId) {
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    // 处理具体的逻辑
//                    stopSelf();
//                }
//            }).start();
//            return super.onStartCommand(intent, flags, startId);
//        }
//    }

}