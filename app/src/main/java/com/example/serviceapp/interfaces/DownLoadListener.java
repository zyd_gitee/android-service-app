package com.example.serviceapp.interfaces;

public interface DownLoadListener {
    //onProgress()方法用于通知当前的下载进度
    void onProgress(int progress);

    // onSuccess()方法用于通知下载成功事件
    void onSuccess();

    // onFailed()方法用于通知下载失败事件
    void onFailed();

    // onPaused()方法用于通知下载暂停事件
    void onPaused();

    // onCanceled()方法用于通知下载取消事件
    void onCanceled();

}
