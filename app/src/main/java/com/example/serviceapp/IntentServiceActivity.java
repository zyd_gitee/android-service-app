package com.example.serviceapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.serviceapp.services.MyIntentService;
import androidx.appcompat.app.AppCompatActivity;

public class IntentServiceActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_service);
        Button startIntentService = (Button) findViewById(R.id.start_intent_service);
        Button stopIntentService = (Button) findViewById(R.id.stop_intent_service);
        startIntentService.setOnClickListener(this);
        stopIntentService.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.start_intent_service:
                Log.d("MyIntentService activity start","Thread id is="+Thread.currentThread().getId());
                Intent StartIntent = new Intent(this, MyIntentService.class);
                startService(StartIntent);
                break;
            case R.id.stop_intent_service:
                Log.d("MyIntentService activity stop","Thread id is="+Thread.currentThread().getId());
                break;

        }
    }
}