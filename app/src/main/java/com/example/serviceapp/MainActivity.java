package com.example.serviceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.serviceapp.services.MyService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button startService = (Button) findViewById(R.id.start_service);
        Button stopService = (Button) findViewById(R.id.stop_service);
        startService.setOnClickListener(this);
        stopService.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_service:
                /**
                 * android8.0以上如果应用程序在后台运行，则不允许在后台启动服务。
                 * Android 要求我们显式地启动服务，context.startForegroundService
                 * 而不是context.startService当服务在 5 秒内启动时，它必须与通知相关联，以便有一个与之关联的 UI 元素。
                 */
                Intent startIntent = new Intent(this, MyService.class);
                startService(startIntent);// 启动服务
                break;
            case R.id.stop_service:
                Intent stopIntent = new Intent(this, MyService.class);
                stopService(stopIntent); // 停止服务
                break;
            default:
                break;
        }
    }
}